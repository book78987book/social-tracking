# social-tracking

> tracking user activities in social platforms like [bilibili](https://www.bilibili.com/), [weibo](https://weibo.com/)

### Quick Start

1. Complete environment variables in `.env`
1. `docker-compose up -d`

### Features

1. subscribe / unsubscribe on [telegram](https://telegram.org/)
1. tracking user's bilibili dynamic
1. tracking user's bilibili live status
