package main

import "github.com/jinzhu/gorm"

// models begin
type Tracking struct {
	gorm.Model
	Uid BiliUid `gorm:"column:uid"`
}

type Dynamic struct {
	gorm.Model
	Uid BiliUid `gorm:"column:uid"`
	Did string  `gorm:"column:did"`
}

type Live struct {
	gorm.Model
	Uid    BiliUid `gorm:"column:uid"`
	Status int     `gorm:"column:status"`
}

// models end

// subscription RPC types begin

type SubArgs struct{}
type SubReply []BiliUid
type Subscription map[BiliUid]TgUsers

// subscription RPC types end

// Postman RPC types begin
type BiliVideo struct {
	VideoSrc   string
	VideoTitle string
}

type BiliMedia struct {
	PicSrcs []string
	Video   BiliVideo
}

type RawMsg struct {
	User    BiliUser
	Content string
	Media   BiliMedia
}

type BiliUid int64
type BiliUserName string

type BiliUser struct {
	Uid  BiliUid
	Name BiliUserName
}

type PubArgs struct {
	Data RawMsg
}

type PubReply struct{}

// Postman RPC types end

// TG types begin

type TgUserName string
type TgChatId int64
type TgUsers map[TgUserName]TgChatId

// TG types end

type DynamicResp struct {
	Code    int         `json:"code"`
	Msg     string      `json:"msg"`
	Message string      `json:"message"`
	Dynamic DynamicData `json:"data"`
}

type LiveResp struct {
	Code    int      `json:"code"`
	Message string   `json:"message"`
	Live    LiveData `json:"data"`
}

type LiveData struct {
	LiveStatus int    `json:"liveStatus"`
	Title      string `json:"title"`
}

type DynamicData struct {
	HasMore    int            `json:"has_more"`
	Cards      []*DynamicCard `json:"cards"`
	NextOffset int64          `json:"next_offset"`
}

type DynamicCard struct {
	Desc       DynamicDesc `json:"desc"`
	Card       string      `json:"card"`
	ExtendJson string      `json:"extend_json"`
	Extra      interface{} `json:"extra"`
	Display    interface{} `json:"display"`
}

type DynamicDesc struct {
	DynamicIdStr string `json:"dynamic_id_str"`
}

type Card struct {
	User   CardUser `json:"user"`
	Item   CardItem `json:"item"`
	Origin string   `json:"origin"`
}

type CardUser struct {
	Uid     BiliUid      `json:"uid"`
	Uname   BiliUserName `json:"uname"`
	Name    BiliUserName `json:"name"`
	FaceUrl string       `json:"face"`
}
type CardItem struct {
	Content     string    `json:"content"`
	Description string    `json:"description"`
	Pictures    []CardPic `json:"pictures"`
	Ctrl        string    `json:"ctrl"`
	OrigById    int64     `json:"orig_by_id"`
	PreById     int64     `json:"pre_by_id"`
	Reply       int64     `json:"reply"`
	RpId        int64     `json:"rp_id"`
	Timestamp   int64     `json:"timestamp"`
	Uid         int64     `json:"uid"`
}

type CardPic struct {
	ImgSrc string `json:"img_src"`
}
