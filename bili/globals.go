package main

import (
	"net/rpc"

	"github.com/jinzhu/gorm"
)

var rClient *rpc.Client

var db *gorm.DB

var syncChans map[string]map[BiliUid]chan struct{}
