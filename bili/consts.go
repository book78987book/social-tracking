package main

import (
	"fmt"
	"os"
)

var (
	MYSQL_USER     = os.Getenv("MYSQL_USER")
	MYSQL_PASSWORD = os.Getenv("MYSQL_PASSWORD")
	MYSQL_HOST     = os.Getenv("MYSQL_HOST")
	MYSQL_PORT     = os.Getenv("MYSQL_PORT")
	MYSQL_DB       = os.Getenv("MYSQL_DB")
	MYSQL_URL      = fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?parseTime=true", MYSQL_USER, MYSQL_PASSWORD, MYSQL_HOST, MYSQL_PORT, MYSQL_DB)

	botServerAddr = os.Getenv("BOT_RPC_ADDR")
)

const (
	UserDynamicUrlTpl    = "https://api.vc.bilibili.com/dynamic_svr/v1/dynamic_svr/space_history?host_uid=%d&offset_dynamic_id=0&need_top=0"
	UserLiveStatusUrlTpl = "https://api.live.bilibili.com/room/v1/Room/getRoomInfoOld?mid=%d"
)
