package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net"
	"net/http"
	"net/rpc"
	"os"
	"time"

	req "github.com/imroc/req"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

func initSyncChans() {
	syncChans = make(map[string]map[BiliUid]chan struct{})
	syncChans["dynamic"] = make(map[BiliUid]chan struct{})
	syncChans["streaming"] = make(map[BiliUid]chan struct{})
}

func initRPCClient() {
	var err error
	rClient, err = rpc.DialHTTP("tcp", botServerAddr)
	if err != nil {
		log.Fatal("Dialing: ", err)
	}
}

func initORM() {
	var err error
	db, err = gorm.Open("mysql", MYSQL_URL)
	if err != nil {
		panic(err)
	}
	if !db.HasTable(&Dynamic{}) {
		db.CreateTable(&Dynamic{})
	}
	if !db.HasTable(&Live{}) {
		db.CreateTable(&Live{})
	}
	if !db.HasTable(&Tracking{}) {
		db.CreateTable(&Tracking{})
	}
}

func startTracking(uid BiliUid) {
	syncChans["dynamic"][uid] = make(chan struct{}, 1)
	trackUserDynamic(uid, false)
	syncChans["streaming"][uid] = make(chan struct{}, 1)
	trackUserStreaming(uid, false, true)
}

func stopTracking(uid BiliUid) {
	db.Table("dynamics").Where("uid = ?", uid).Unscoped().Delete(Dynamic{})
	db.Table("lives").Where("uid = ?", uid).Unscoped().Delete(Live{})
	delete(syncChans["dynamic"], uid)
	delete(syncChans["streaming"], uid)
}

func initTracking() {
	updateTrackingUids()
	var trackings []*Tracking
	db.Table("trackings").Find(&trackings)
	for _, tracking := range trackings {
		startTracking(tracking.Uid)
	}
}

func init() {
	initSyncChans()
	initRPCClient()
	initORM()
	initTracking()
}

// sub/unsub RPC

func (_ *BiliUid) Subscribe(biliUid BiliUid, reply *struct{}) error {
	var recordCount int64
	db.Table("trackings").Where("uid = ?", biliUid).Count(&recordCount)
	if recordCount == 0 {
		db.Table("trackings").Create(&Tracking{Uid: biliUid})
		startTracking(biliUid)
	}
	return nil
}

func (_ *BiliUid) Unsubscribe(biliUid BiliUid, reply *struct{}) error {
	var recordCount int64
	db.Table("trackings").Where("uid = ?", biliUid).Count(&recordCount)
	if recordCount != 0 {
		db.Table("trackings").Where("uid = ?", biliUid).Unscoped().Delete(Tracking{})
		stopTracking(biliUid)
	}
	return nil
}

// RPC end

func publish(rawMsg RawMsg) {
	var pubArgs PubArgs = PubArgs{rawMsg}
	rClient.Call("BiliUser.Publish", &pubArgs, new(PubReply))
}

func updateTrackingUids() {
	var tracking []BiliUid
	err := rClient.Call("Subscription.UpdateSub", new(SubArgs), &tracking)
	if err != nil {
		log.Println("update tracking list: ", err)
		return
	}
	db.Table("trackings").Unscoped().Delete(&Tracking{})
	for _, uid := range tracking {
		db.FirstOrCreate(&Tracking{}, Tracking{Uid: uid})
	}
	log.Println(tracking)
}

func fetchUserDynamic(uid BiliUid) (*req.Resp, error) {
	c := req.New()
	log.Println("fetching user dynamic: ", uid)
	dynamicUri := fmt.Sprintf(UserDynamicUrlTpl, uid)
	resp, err := c.Get(dynamicUri)
	if err != nil {
		return nil, fmt.Errorf("get data failed: %w", err)
	}
	return resp, nil
}

func extractText(card Card) string {
	text := "> Source: BiliBili"
	var userName BiliUserName
	if card.User.Name != "" {
		userName = card.User.Name
	} else if card.User.Uname != "" {
		userName = card.User.Uname
	}
	text = fmt.Sprintf("%s\n\n@%s:", text, userName)
	if card.Item.Content != "" {
		text = text + card.Item.Content
	} else if card.Item.Description != "" {
		text = text + card.Item.Description
	}
	if card.Origin != "" {
		var originCard Card
		json.Unmarshal([]byte(card.Origin), &originCard)
		var originUserName BiliUserName
		if originCard.User.Name != "" {
			originUserName = originCard.User.Name
		} else if originCard.User.Uname != "" {
			originUserName = originCard.User.Uname
		}
		if originCard.Item.Content != "" {
			text = fmt.Sprintf("%s\n\n@%s:%s", text, originUserName, originCard.Item.Content)
		} else if originCard.Item.Description != "" {
			text = fmt.Sprintf("%s\n\n@%s:%s", text, originUserName, originCard.Item.Description)
		}
	}
	return text
}

func extractMedia(card Card) BiliMedia {
	var pics []string
	if card.Item.Pictures != nil {
		for _, pic := range card.Item.Pictures {
			pics = append(pics, pic.ImgSrc)
		}
	}
	if card.Origin != "" {
		var originCard Card
		json.Unmarshal([]byte(card.Origin), &originCard)
		if originCard.Item.Pictures != nil {
			for _, pic := range originCard.Item.Pictures {
				pics = append(pics, pic.ImgSrc)
			}
		}
	}
	return BiliMedia{
		PicSrcs: pics,
		Video:   BiliVideo{},
	}
}

func extractData(uid BiliUid, dynamicResp *DynamicResp) []RawMsg {
	cards := dynamicResp.Dynamic.Cards
	var rawMsgs []RawMsg
	dynamicIds := []string{}
	for i := len(cards) - 1; i >= 0; i-- {
		card := cards[i]
		dynamicIds = append(dynamicIds, card.Desc.DynamicIdStr)
		var recordCount int64
		db.Table("dynamics").Where("uid = ? and did = ?", uid, card.Desc.DynamicIdStr).Count(&recordCount)
		if recordCount != 0 {
			continue
		}
		var _card Card
		json.Unmarshal([]byte(card.Card), &_card)
		rawMsg := RawMsg{
			BiliUser{_card.User.Uid, _card.User.Name},
			extractText(_card),
			extractMedia(_card),
		}
		rawMsgs = append(rawMsgs, rawMsg)
	}
	db.Table("dynamics").Where("uid = ?", uid).Unscoped().Delete(Dynamic{})
	for _, dynamicId := range dynamicIds {
		db.Table("dynamics").Create(&Dynamic{Uid: uid, Did: dynamicId})
	}
	return rawMsgs
}

func publishAll(rawMsgs []RawMsg) error {
	log.Println("publish all: ", rawMsgs)
	for _, rawMsg := range rawMsgs {
		rawMsg := rawMsg
		go publish(rawMsg)
	}
	return nil
}

func trackUserDynamic(uid BiliUid, pubImmidiatly bool) error {
	// publish("558070436", RawMsg{fmt.Sprintf("tracking uid %s dynamic", uid)})
	syncChans["dynamic"][uid] <- struct{}{}
	defer func() {
		<-syncChans["dynamic"][uid]
	}()
	resp, err := fetchUserDynamic(uid)
	if err != nil {
		return fmt.Errorf("fetch user dynamic failed: %w", err)
	}
	var body DynamicResp
	err = resp.ToJSON(&body)
	if err != nil {
		return fmt.Errorf("parse response to dynamic data failed: %w", err)
	}
	if body.Code != 0 {
		return fmt.Errorf("response returned non-zero code: %v", body)
	}
	rawMsgs := extractData(uid, &body)
	if pubImmidiatly {
		publishAll(rawMsgs)
	}
	return nil
}

func fetchLiveData(uid BiliUid) (*req.Resp, error) {
	c := req.New()
	log.Println("fetch user live data: ", uid)
	dynamicUri := fmt.Sprintf(UserLiveStatusUrlTpl, uid)
	resp, err := c.Get(dynamicUri)
	if err != nil {
		return nil, fmt.Errorf("get data failed: %w", err)
	}
	return resp, nil
}

func extractLiveStatus(uid BiliUid, liveResp *LiveResp) []RawMsg {
	liveData := liveResp.Live
	if liveData.LiveStatus == 1 {
		content := fmt.Sprintf("%d is streaming: %s", uid, liveData.Title)
		return []RawMsg{RawMsg{BiliUser{uid, ""}, content, BiliMedia{}}}
	}
	return []RawMsg{}
}

func trackUserStreaming(uid BiliUid, pubImmidiatly, forceUpdate bool) error {
	// publish("558070436", RawMsg{fmt.Sprintf("tracking uid %s streaming", uid)})
	syncChans["streaming"][uid] <- struct{}{}
	defer func() {
		<-syncChans["streaming"][uid]
	}()
	if !forceUpdate {
		var recordCount int64
		db.Table("lives").Where("uid = ? and status = 1", uid).Count(&recordCount)
		if recordCount != 0 {
			return nil
		}
	}
	resp, err := fetchLiveData(uid)
	if err != nil {
		return fmt.Errorf("fetch live status failed: %w", err)
	}
	var body LiveResp
	if err := resp.ToJSON(&body); err != nil {
		log.Println(err)
		return fmt.Errorf("parse response to live data failed: %w", err)
	}
	if forceUpdate {
		db.Table("lives").Create(&Live{Uid: uid, Status: 0})
	}
	if body.Live.LiveStatus == 1 {
		rawMsgs := extractLiveStatus(uid, &body)
		var live Live
		db.Table("lives").Where("uid = ? and status = 0", uid).First(&live)
		live.Status = body.Live.LiveStatus
		db.Save(&live)
		if pubImmidiatly {
			publishAll(rawMsgs)
		}
	}
	return nil
}

func tracking() {
	var trackings []*Tracking
	db.Find(&trackings)
	for _, tracking := range trackings {
		go trackUserDynamic(tracking.Uid, true)
		go trackUserStreaming(tracking.Uid, true, false)
	}
}

func main() {
	go func() {
		biliUid := new(BiliUid)
		rpc.Register(biliUid)
		rpc.HandleHTTP()
		rpcAddr := os.Getenv("BILI_RPC_ADDR")
		l, e := net.Listen("tcp", rpcAddr)
		if e != nil {
			log.Fatal("listen error: ", e)
		}
		log.Println("RPC listening on ", rpcAddr)
		http.Serve(l, nil)
	}()

	exitChan := make(chan os.Signal)
	ticks := time.Tick(10 * time.Second)
	for {
		select {
		case <-exitChan:
			os.Exit(0)
		case <-ticks:
			updateTrackingUids()
			tracking()
		}
	}
}
