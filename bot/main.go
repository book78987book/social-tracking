package main

import (
	"fmt"
	"log"
	"os"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

func init() {
	log.Println(MYSQL_URL, biliServerAddr)
	var err error
	bot, err = tgbotapi.NewBotAPI(APIToken)
	if err != nil {
		log.Fatal(fmt.Errorf("create bot api failed: %w", err))
	}
	log.Println("create bot api success")

	bot.Debug = true
	db, err = gorm.Open("mysql", MYSQL_URL)
	if err != nil {
		panic(err)
	}
	if !db.HasTable(&Sub{}) {
		db.CreateTable(&Sub{})
	}
}

func main() {
	exitChan := make(chan os.Signal)

	go reception()
	go postman()

	<-exitChan
}
