package main

// Commands
const (
	SUBSCRIBE   = "/subscribe"
	UNSUBSCRIBE = "/unsubscribe"
)

// MessageType
const (
	BOTCOMMAND = "bot_command"
)

// tracking group chat id
const (
	TrackingGroupId = -1001437213546
)
