package main

import (
	"fmt"
	"log"
	"net/rpc"
	"sync"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
)

func subscribe(chatId TgChatId, biliUid BiliUid) {
	var recordCount int64
	db.Table("subs").Where("uid = ?", biliUid).Count(&recordCount)
	if recordCount == 0 {
		rClient, err := rpc.DialHTTP("tcp", biliServerAddr)
		if err != nil {
			log.Fatal(fmt.Errorf("Dialing: %w", err))
		}
		err = rClient.Call("BiliUid.Subscribe", biliUid, new(struct{}))
		if err != nil {
			log.Println(fmt.Errorf("Subscribe bilibili failed: %w", err))
		}
	}
	db.Table("subs").Create(&Sub{Uid: biliUid, ChatId: chatId})
	content := fmt.Sprintf("subscribe uid %d success", biliUid)
	sendMessage([]TgChatId{chatId}, RawMsg{Content: content})
}

func unsubscribe(chatId TgChatId, biliUid BiliUid) {
	db.Table("subs").Where("uid = ? and chatid = ?", biliUid, chatId).Unscoped().Delete(Sub{})
	var recordCount int64
	db.Table("subs").Where("uid = ?", biliUid).Count(&recordCount)
	if recordCount == 0 {
		// stop tracking user's activity
		rClient, err := rpc.DialHTTP("tcp", biliServerAddr)
		if err != nil {
			log.Fatal(fmt.Errorf("Dialing: %w", err))
		}
		err = rClient.Call("BiliUid.Unsubscribe", biliUid, new(struct{}))
		if err != nil {
			log.Println(fmt.Errorf("Unsubscribe bilibili failed: %w", err))
		}
	}
	content := fmt.Sprintf("unsubscribe uid %d success", biliUid)
	sendMessage([]TgChatId{chatId}, RawMsg{Content: content})
}

func sendMessage(chatIds []TgChatId, rawMsg RawMsg) {

	chatIds = append(chatIds, TrackingGroupId)

	var wg sync.WaitGroup
	for _, chatId := range chatIds {
		wg.Add(1)
		chatId := chatId
		go func() {
			defer func() { wg.Done() }()
			textMsg := tgbotapi.NewMessage(int64(chatId), rawMsg.Content)
			var msgResp tgbotapi.Message
			var err error
			retry(10, 5, 10, func() error {
				msgResp, err = bot.Send(textMsg)
				if err != nil {
					return fmt.Errorf("%w", err)
				}
				return nil
			})
			if rawMsg.Media.PicSrcs != nil {
				var mediaGroup []interface{}
				for _, media := range rawMsg.Media.PicSrcs {
					mediaGroup = append(mediaGroup, tgbotapi.NewInputMediaPhoto(media))
				}
				mediaGroupMsg := tgbotapi.NewMediaGroup(int64(chatId), mediaGroup)
				mediaGroupMsg.BaseChat.ReplyToMessageID = msgResp.MessageID
				log.Println(mediaGroupMsg)
				retry(10, 5, 10, func() error {
					_, err := bot.Send(mediaGroupMsg)
					if err != nil {
						return fmt.Errorf("%w", err)
					}
					return nil
				})
			}
		}()
	}
	wg.Wait()
}
