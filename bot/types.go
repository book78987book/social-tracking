package main

import "github.com/jinzhu/gorm"

// bili basic types begin
type BiliVideo struct {
	VideoSrc   string
	VideoTitle string
}

type BiliMedia struct {
	PicSrcs []string
	Video   BiliVideo
}

type BiliUid int64
type BiliUserName string

type BiliUser struct {
	Uid  BiliUid
	Name BiliUserName
}

// bili basic types end

// bili sub update RPC types begin

type SubArgs struct{}
type SubReply []BiliUid
type Subscription map[BiliUid]TgUsers

// bili sub update RPC types end

// models begin
type Sub struct {
	gorm.Model
	Uid    BiliUid  `gorm:"column:uid"`
	ChatId TgChatId `gorm:"column:chatid"`
}

// models end

// postman RPC types begin

type PubArgs struct {
	Data RawMsg
}

type PubReply struct{}

type RawMsg struct {
	User    BiliUser
	Content string
	Media   BiliMedia
}

// postman RPC types end

// TG types begin

type TgUserName string
type TgChatId int64
type TgUsers map[TgUserName]TgChatId

// TG types end
