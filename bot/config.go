package main

import (
	"fmt"
	"os"
)

var (
	APIToken = os.Getenv("TG_BOT_API_TOKEN")

	MYSQL_USER     = os.Getenv("MYSQL_USER")
	MYSQL_PASSWORD = os.Getenv("MYSQL_PASSWORD")
	MYSQL_HOST     = os.Getenv("MYSQL_HOST")
	MYSQL_PORT     = os.Getenv("MYSQL_PORT")
	MYSQL_DB       = os.Getenv("MYSQL_DB")
	MYSQL_URL      = fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?parseTime=true", MYSQL_USER, MYSQL_PASSWORD, MYSQL_HOST, MYSQL_PORT, MYSQL_DB)

	biliServerAddr = os.Getenv("BILI_RPC_ADDR")
)
