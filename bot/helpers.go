package main

import (
	"fmt"
	"log"
	"strconv"
	"strings"
	"time"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
)

func cannotUnderstandMessage(msg *tgbotapi.Message) error {
	// entities must contain only one item
	if msg.Entities == nil || len(*msg.Entities) != 1 {
		return fmt.Errorf("Empty entities or multi entities exists: %v", msg.Entities)
	}
	// currently handle specific bot_command only
	entity := (*msg.Entities)[0]
	if entity.Type != BOTCOMMAND || entity.Offset != 0 {
		return fmt.Errorf("Entity is not bot_command or offset is not 0: %v", entity)
	}
	// currently handle SUBSCRIBE/UNSUBSCRIBE only
	textSplited := strings.SplitN(msg.Text, " ", 2)
	if len(textSplited) < 2 {
		return fmt.Errorf("Missing args: bilibili uid")
	}
	command, biliUid := textSplited[0], textSplited[1]
	switch command {
	case SUBSCRIBE, UNSUBSCRIBE:
	default:
		return fmt.Errorf("Unknown command: %v", command)
	}
	// accept numeric bilibili uid only
	_, err := strconv.Atoi(biliUid)
	if err != nil {
		return fmt.Errorf("Assert integer failed: %w", err)
	}
	return nil
}

func retry(sleep, times, backoff int, f func() error) {
	err := f()
	for i := 0; i < times; i++ {
		if err == nil {
			break
		}
		log.Println(fmt.Errorf("Error: %w", err))
		time.Sleep(time.Duration(sleep+backoff*i) * time.Second)
		err = f()
	}
}
