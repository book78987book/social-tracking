module gitlab.com/book78987book/social-tracking/bot

go 1.14

require (
	github.com/go-telegram-bot-api/telegram-bot-api v4.6.4+incompatible
	github.com/jinzhu/gorm v1.9.15
	github.com/technoweenie/multipartstreamer v1.0.1 // indirect
)
