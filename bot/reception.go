package main

import (
	"fmt"
	"log"
	"strconv"
	"strings"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
)

func reception() {

	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	updates, err := bot.GetUpdatesChan(u)
	if err != nil {
		panic(fmt.Errorf("get update chan faileld: %w", err))
	}

	log.Println("receiving updates...")
	for update := range updates {
		// ignore empty message
		if update.Message == nil && update.ChannelPost == nil {
			continue
		}

		// ignore wrong syntax
		var message *tgbotapi.Message
		if update.Message != nil {
			message = update.Message
		} else if update.ChannelPost != nil {
			message = update.ChannelPost
		}
		if err := cannotUnderstandMessage(message); err != nil {
			msg := tgbotapi.NewMessage(update.Message.Chat.ID, fmt.Sprint(err))
			msg.ReplyToMessageID = update.Message.MessageID
			bot.Send(msg)
			continue
		}

		chatId := message.Chat.ID
		msgContent := message.Text
		msgSplited := strings.Split(msgContent, " ")
		command, biliUidStr := msgSplited[0], msgSplited[1]
		biliUid, _ := strconv.Atoi(biliUidStr)
		switch command {
		case SUBSCRIBE:
			subscribe(TgChatId(chatId), BiliUid(int64(biliUid)))
		case UNSUBSCRIBE:
			unsubscribe(TgChatId(chatId), BiliUid(int64(biliUid)))
		}
	}
}
