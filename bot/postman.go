package main

import (
	"log"
	"net"
	"net/http"
	"net/rpc"
	"os"
)

func (biliUser *BiliUser) Publish(args *PubArgs, reply *PubReply) error {
	msgData := args.Data
	var records []Sub
	db.Table("subs").Where("uid = ?", msgData.User.Uid).Find(&records)
	if len(records) == 0 {
		return nil
	}
	chatIds := []TgChatId{}
	for _, record := range records {
		chatIds = append(chatIds, record.ChatId)
	}

	sendMessage(chatIds, args.Data)
	return nil
}

func (sub *Subscription) UpdateSub(args *SubArgs, reply *SubReply) error {
	var subs []*Sub
	db.Table("subs").Select("distinct(uid)").Find(&subs)
	for _, sub := range subs {
		*reply = append(*reply, sub.Uid)
	}
	return nil
}

func postman() {
	biliUser := new(BiliUser)
	sub := new(Subscription)
	rpc.Register(biliUser)
	rpc.Register(sub)
	rpc.HandleHTTP()
	rpcAddr := os.Getenv("BOT_RPC_ADDR")
	l, e := net.Listen("tcp", rpcAddr)
	if e != nil {
		log.Fatal("listen error:", e)
	}
	log.Printf("RPC listening on %s\n", rpcAddr)
	http.Serve(l, nil)
}
